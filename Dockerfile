FROM alpine

RUN apk add unzip

RUN apk update && apk add curl && apk add python3 && \
    curl https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz | tar zx && \
    ./google-cloud-sdk/install.sh --usage-reporting=false --path-update=true && \
    apk add bash # Add bash to the container && apk add sh

RUN apk add --update --no-cache curl \
    && curl -LO https://releases.hashicorp.com/terraform/1.0.7/terraform_1.0.7_linux_amd64.zip \
    && unzip terraform_1.0.7_linux_amd64.zip -d /usr/bin \
    && rm terraform_1.0.7_linux_amd64.zip

ENV PATH $PATH:/google-cloud-sdk/bin

RUN gcloud --version
RUN echo "This is bash"
RUN terraform --version
